configfile: "templates/final.yaml"

rule jaccard:
    input:
        lambda wildcards: config["samples"][wildcards.sample]
    output:
        jaccard="output/{sample}_jaccard.Rdata"
    conda:
        "envs/longreadselection.yaml"
    params:
        threads=config["threads"],
        seed = config["seed"]
    message:
        "Calculating Jaccard distances from {input}"
    log:
        "logs/{sample}_log_jaccard.txt"
    script:
        "scripts/jaccard.R"

rule tsne:
    input:
        jaccard="output/{sample}_jaccard.Rdata"
    output:
        rtsne="output/{sample}_rtsne.Rdata"
    params:
        seed = config["seed"]
    conda:
        "envs/longreadselection.yaml"
    message:
        "Running tsne with a perplexity value of 30"
    log:
        "logs/{sample}_log_rtsne.txt"
    script:
        "scripts/Rtsne.R"

rule kmeans:
    input:
        rtsne="output/{sample}_rtsne.Rdata"
    output:
        tsne_centroids="output/{sample}_tsne_centroids.png",
        selected_isolates="output/{sample}_selected_isolates.csv"
    params:
        seed=config["seed"],
        centers=config["centers"]
    conda:
        "envs/longreadselection.yaml"
    message:
        "Running the k-means algorithm"
    log:
        "logs/{sample}_log_rtsne.txt"
    script:
        "scripts/kmeans.R"
