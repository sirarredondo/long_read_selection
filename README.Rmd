---
title: "Selection of isolates for long-read sequencing based on gene presence/absence data"
output: 
  github_document
fontsize: 16pt
editor_options: 
  chunk_output_type: inline
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Introduction

- Do you have a large collection of short-read bacterial isolates? 
- Do you have some budget to complete ONLY A FEW genomes with long-read sequencing?  

If that's the case, you probably need to select somehow which isolates are going to be long-read sequenced

We propose the following approach: 

- Consider the gene presence/absence matrix (.Rtab file) of orthologous genes computed by [Roary](https://sanger-pathogens.github.io/Roary/) or [Panaroo](https://github.com/gtonkinhill/panaroo)
- Transform the matrix into a distance matrix considering Jaccard distances. parDist function in the R package [parallelDist](https://github.com/alexeckert/parallelDist) (version 0.2.4)
- Reduce the dimensionality distance matrix into 2D using the [tsne algorithm](https://lvdmaaten.github.io/tsne/) considering a default perplexity value of 30. Rtsne function in the R package [Rtsne](https://github.com/jkrijthe/Rtsne) (version 0.15)
- Run the [k-means algorithm](https://www.cs.utexas.edu/~kuipers/readings/Alsabti-hpdm-98.pdf) indicating the number of centroids corresponding to the number of desired isolates that you want to long-read sequence. kmeans function implemented in the R package stats (version 3.6.3)
- For each centroid, select the isolate with the lowest Euclidean distance. dist function implemented in the R package stats (version 3.6.3)

# Installation 

The pipeline relies on Conda and Snakemake. Thus, we require you to run the pipeline in a conda environment with Snakemake installed. 

1. [Conda](https://bioconda.github.io/) 
2. [Snakemake](https://snakemake.readthedocs.io/en/stable/) version 5.5.4

So basically if you run in the terminal the following and you see the help page of both tools, you are ready to go!

```{bash, eval = FALSE}
conda -h 
snakemake -h 
```

# Important: gcc version 

The gcc version (gnu compiler) affects the tsne results obtained for an identical gene presence/absence file. Thus, the pipeline will generate distinct results depending on the gcc version that you are running. Please keep this in mind, if you need to reproduce the same results run the same gcc version. You can install a particular gcc version using conda even if you are using a different gnu compiler in your system.  

Check your gcc version with: 

```{bash, eval = FALSE}
gcc --version
```


# Basic Usage

You only need to indicate the path to the gene_presence_absence.file (-i argument) and the number of isolates to be selected (-c argument). For example, if we want to select 12 isolates, we should run the pipeline in the following way:

```{bash, eval = FALSE}
./selection.sh -i example/gene_presence_absence.Rtab -c 12
```

# Complete usage

```{bash, eval = FALSE}
./selection.sh -h 
```

Long-read selection based on gene presence/absence data:

Basic usage example: ./selection.sh -i gene_presence_absence.Rtab -c 12'

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -i 	 Mandatory: Path to the file with the gene presence/absence matrix. Rtab format
 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  -c 	 Mandatory: Number of isolates to be selected for long-read sequencing. Positive integer number
 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -o 	 Optional: Output name of the files generated. String character. Default: 'selection + day_month_year'

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -t 	 Optional: Number of threads to run the snakemake pipeline. Positive integer number. Default: 1

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -s 	 Optional: Seed to fix the randomness for the tsne clustering. Positive integer number. Default: 123


# Output 

The pipeline will create a folder called 'output' with two main files:

## *selected_isolates.csv 

This comma-separated file contains three columns: 

- First column termed 'Kmeans_centroid' indicates the centroid (group) defined by kmeans. You can consider this as a cluster name
- Third column termed 'Selected_isolate' indicates the name of the isolate selected for that particular group/cluster 
- Second column termed 'Euclidean_distance' contains the distance between the centroid and the selected isolate 

The third column contains the name of the isolates selected by the pipeline for long-read sequencing

```{r, echo = FALSE}
selection <- read.csv(file = 'output/example_selected_isolates.csv', header = TRUE)

knitr::kable(selection)
```



## *tsne_centroids.png

Plot of the tsne clustering indicating the final position of the centroids determined by k-means and the isolate selected for long-read sequencing. In red, you should see the position of the centroids in the 2D tsne plot. In blue, you should see the position of the isolate closest (Euclidean distance) to each centroid (red diamond). In black, you should observe the position of the rest of the isolates (not selected for long-read sequencing)

```{r, echo=FALSE}
knitr::include_graphics("output/example_tsne_centroids.png")
```

# Reproducibility (tsne)

In the folder 'output', you will find the following file:

- *_rtsne.Rdata 

This file contains a workspace with the tsne object generated with Rtsne. You can load this object into your R session to retrieve the 2D matrix created by Rtsne. This embedding plot can be useful to generate subsequent analyses or rerun the k-means algorithm in another machine/server 


# Issues/Bugs 

You can report any issues or bugs that you find while installing/running the pipeline using the [issue tracker](https://gitlab.com/sirarredondo/long_read_selection/issues)

