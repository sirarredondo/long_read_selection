Selection of isolates for long-read sequencing based on gene
presence/absence data
================

# Introduction

-   Do you have a large collection of short-read bacterial isolates?
-   Do you have some budget to complete ONLY A FEW genomes with
    long-read sequencing?

If that’s the case, you probably need to select somehow which isolates
are going to be long-read sequenced

We propose the following approach:

-   Consider the gene presence/absence matrix (.Rtab file) of
    orthologous genes computed by
    [Roary](https://sanger-pathogens.github.io/Roary/) or
    [Panaroo](https://github.com/gtonkinhill/panaroo)
-   Transform the matrix into a distance matrix considering Jaccard
    distances. parDist function in the R package
    [parallelDist](https://github.com/alexeckert/parallelDist) (version
    0.2.4)
-   Reduce the dimensionality distance matrix into 2D using the [tsne
    algorithm](https://lvdmaaten.github.io/tsne/) considering a default
    perplexity value of 30. Rtsne function in the R package
    [Rtsne](https://github.com/jkrijthe/Rtsne) (version 0.15)
-   Run the [k-means
    algorithm](https://www.cs.utexas.edu/~kuipers/readings/Alsabti-hpdm-98.pdf)
    indicating the number of centroids corresponding to the number of
    desired isolates that you want to long-read sequence. kmeans
    function implemented in the R package stats (version 3.6.3)
-   For each centroid, select the isolate with the lowest Euclidean
    distance. dist function implemented in the R package stats (version
    3.6.3)

# Installation

The pipeline relies on Conda and Snakemake. Thus, we require you to run
the pipeline in a conda environment with Snakemake installed.

1.  [Conda](https://bioconda.github.io/)
2.  [Snakemake](https://snakemake.readthedocs.io/en/stable/) version
    5.5.4

So basically if you run in the terminal the following and you see the
help page of both tools, you are ready to go!

``` bash
conda -h 
snakemake -h 
```

# Important: gcc version

The gcc version (gnu compiler) affects the tsne results obtained for an
identical gene presence/absence file. Thus, the pipeline will generate
distinct results depending on the gcc version that you are running.
Please keep this in mind, if you need to reproduce the same results run
the same gcc version. You can install a particular gcc version using
conda even if you are using a different gnu compiler in your system.

Check your gcc version with:

``` bash
gcc --version
```

# Basic Usage

You only need to indicate the path to the gene\_presence\_absence.file
(-i argument) and the number of isolates to be selected (-c argument).
For example, if we want to select 12 isolates, we should run the
pipeline in the following way:

``` bash
./selection.sh -i example/gene_presence_absence.Rtab -c 12
```

# Complete usage

``` bash
./selection.sh -h 
```

Long-read selection based on gene presence/absence data:

Basic usage example: ./selection.sh -i gene\_presence\_absence.Rtab -c
12’

       -i Mandatory: Path to the file with the gene presence/absence
matrix. Rtab format

       -c Mandatory: Number of isolates to be selected for long-read
sequencing. Positive integer number

       -o Optional: Output name of the files generated. String
character. Default: ‘selection + day\_month\_year’

       -t Optional: Number of threads to run the snakemake pipeline.
Positive integer number. Default: 1

       -s Optional: Seed to fix the randomness for the tsne clustering.
Positive integer number. Default: 123

# Output

The pipeline will create a folder called ‘output’ with two main files:

## \*selected\_isolates.csv

This comma-separated file contains three columns:

-   First column termed ‘Kmeans\_centroid’ indicates the centroid
    (group) defined by kmeans. You can consider this as a cluster name
-   Third column termed ‘Selected\_isolate’ indicates the name of the
    isolate selected for that particular group/cluster
-   Second column termed ‘Euclidean\_distance’ contains the distance
    between the centroid and the selected isolate

The third column contains the name of the isolates selected by the
pipeline for long-read sequencing

| Kmeans\_centroid | Euclidean\_distance | Selected\_isolate |
|:-----------------|--------------------:|:------------------|
| centroid\_1      |           0.3386319 | isolate.120       |
| centroid\_2      |           0.2086251 | isolate.81        |
| centroid\_3      |           0.2653530 | isolate.45        |
| centroid\_4      |           0.8675438 | isolate.92        |
| centroid\_5      |           0.7681943 | isolate.139       |
| centroid\_6      |           0.5561857 | isolate.48        |
| centroid\_7      |           1.0333425 | isolate.311       |
| centroid\_8      |           0.9195832 | isolate.55        |
| centroid\_9      |           2.1497904 | isolate.148       |
| centroid\_10     |           0.5986091 | isolate.6         |
| centroid\_11     |           0.6480100 | isolate.137       |
| centroid\_12     |           0.0866231 | isolate.53        |

## \*tsne\_centroids.png

Plot of the tsne clustering indicating the final position of the
centroids determined by k-means and the isolate selected for long-read
sequencing. In red, you should see the position of the centroids in the
2D tsne plot. In blue, you should see the position of the isolate
closest (Euclidean distance) to each centroid (red diamond). In black,
you should observe the position of the rest of the isolates (not
selected for long-read sequencing)

![](output/example_tsne_centroids.png)<!-- -->

# Reproducibility (tsne)

In the folder ‘output’, you will find the following file:

-   \*\_rtsne.Rdata

This file contains a workspace with the tsne object generated with
Rtsne. You can load this object into your R session to retrieve the 2D
matrix created by Rtsne. This embedding plot can be useful to generate
subsequent analyses or rerun the k-means algorithm in another
machine/server

# Issues/Bugs

You can report any issues or bugs that you find while installing/running
the pipeline using the [issue
tracker](https://gitlab.com/sirarredondo/long_read_selection/issues)
