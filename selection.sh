#!/bin/bash

##to debug
#set -e
#set -v
#set -x

########################################
## A bash script to run long-read selection

while getopts ":i:o:s:c:t:h" opt; do
 case $opt in
   h)
   echo -e "Long-read selection based on gene presence/absence data:\n"
   echo -e "Basic usage example: ./selection.sh -i gene_presence_absence.Rtab -c 12'\n"
   echo -e "Input:\n \t -i \t Mandatory: Path to the file with the gene presence/absence matrix created by pangenome tools such as Roary or Panaroo. Rtab format\n"
   echo -e "Number of isolates to select:\n \t -c \t Mandatory: Number of isolates to be selected for long-read sequencing. Positive integer number\n"
   echo -e "Output name:\n \t -o \t Optional: Output name of the files generated. String character. Default: 'selection + day_month_year'\n"
   echo -e "Threads: \n \t -t \t Optional: Number of threads to run the snakemake pipeline. Positive integer number. Default: 1\n"
   echo -e "Tsne seed: \n \t -s \t Optional: Seed to fix the randomness for the tsne clustering. Positive integer number. Default: 123\n"
   exit
   ;;
   i)
     input=$OPTARG
     ;;
   o)
     name=$OPTARG
     ;;
   s)
     seed=$OPTARG
     ;;
   c)
     centers=$OPTARG
     ;;
   t)
     threads=$OPTARG
     ;;
   h)
     help=$OPTARG
     ;;
   \?)
     ./selection.sh -h
     echo -e "\n"
     echo "Invalid option: -$OPTARG" >&2
     echo "The script requires a single input corresponding to the gene presence/absence file (.Rtab), use the following syntax:"
     exit 1
     ;;
   :)
     ./selection.sh -h
     echo -e "\n"
     echo "Error: Option -$OPTARG requires an argument." >&2
     exit 1
     ;;
 esac
done

if [ -z "$input" ];
then
    ./selection.sh -h
    echo -e "\n Error: Oops, it seems that you are missing the gene presence/absence file (.Rtab).\n"
    exit
fi

if [ -z "$centers" ];
then
    ./selection.sh -h
    echo -e "\n Error: Oops, it seems that you did not indicate the desired number of isolates to select.\n"
    exit
fi
echo -e "Welcome to this selection of isolates for long-read sequencing \n"
echo -e "This is your gene presence/absence file:" $input "\n"

sleep 3s

if [ -z "$name" ];
then
    particular_date=$(date '+%d-%m-%y')
    name=$(echo "selection"_$particular_date)
    echo -e "You did not pass an output name. Your results will be named as" $name "\n"
else
  echo -e "Your results will be named:" $name "\n"
fi

if [ -z "$threads" ];
then
    threads=1
    echo -e "You did not specify the number of threads. Using a single thread \n"
else
  echo -e "Using" $threads "threads" "\n"
fi

if [ -z "$seed" ];
then
    seed="123"
    echo -e "You did not specify a seed to fix the randomness in the tsne clustering. The following seed has been considered" $seed "\n"
else
  echo -e "To fix the randomness in the tsne clustering, the following seed has been considered" $seed "\n"
fi

echo "##################################################################"

( echo "cat <<EOF >templates/final.yaml";
  cat templates/template.yml;
  echo "EOF";
) > templates/temp.yaml
. templates/temp.yaml

sleep 3s

snakemake --unlock --cores $threads --use-conda -s longreadselection.smk output/"$name"_selected_isolates.csv
snakemake --use-conda --cores $threads -s longreadselection.smk output/"$name"_selected_isolates.csv

echo -e "Important! These results are generated with the following gnu compiler"
gcc --version 
